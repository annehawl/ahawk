It is easy to write about what you really experienced! And if the events you describe are your own journey filled with adventure, bright impressions, new acquaintances - it is also extremely pleasant! It is so easy -
Remember to relive, and then - describe, as if flipping through an album with photos. 
I have not carried for years the idea that one day I will write a book! 

### It just happened so.
In my youth I wrote poems - who doesn't write them when I was 16? At lessons of the literature at school before a hoarseness discussed with the favourite teacher works of great authors, I took part in various literary competitions of compositions in school, city, republic. 
But the book - there was not even a thought about it, until...
Until a journey that I wanted to write about happened in my life. With me was my most faithful friend, without whom there would be no travel or book! All roads described in the book, we walked together, hand in hand, on one breath!

The first chapters - then they were not chapters yet, but just a solid text - were born by themselves. It was a small present to a friend - a reminder of one of the episodes of our trip. The friend read it and said: "More!" Several pieces gradually lined up in the story, which had no name. Then, for reading.

### My sister joined in, who also became the heroine of some episodes.

And then the stories became close within the story. In one of the Internet resources, we (me and both the heroines of the story) created the so-called "Literary circle", where I sent fresh sentences, paragraphs, chapters. As a result, quite unexpectedly for myself, I got an adventure novel. It went a long way from an electronic document to a real paper book, but now it is a book written by me, really exists!
Do I consider myself a writer at [essayhawk](https://www.essayhawk.com/)? Not yet! Does this activity bring me pleasure? 
Absolutely! It was worth trying to bring the joy of reading to the general public, but to my family and friends, even if not yet! In addition, a book written once about the journey just screams to me that I can not stop there!
You need to travel, learn languages, learn new things, value friendship and family! My book is my motivator, my engine, my passion!